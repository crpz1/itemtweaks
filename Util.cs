﻿using StardewValley.Menus;

namespace ItemTweaks
{
    public class Util
    {
        public static bool IsInventory(IClickableMenu m)
        {
            if (m is GameMenu menu)
            {
                if (menu.GetCurrentPage() is InventoryPage inventoryPage)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
