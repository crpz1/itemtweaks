﻿using StardewModdingAPI;
using StardewModdingAPI.Events;
using StardewModdingAPI.Utilities;
using StardewValley.Menus;
using StardewValley.Objects;
using StardewValley;
using System.Collections;
using System.Collections.Generic;

namespace ItemTweaks
{
    /// <summary>The mod entry point.</summary>
    public class ModEntry : Mod
    {
        /*********
        ** Public methods
        *********/
        /// <summary>The mod entry point, called after the mod is first loaded.</summary>
        /// <param name="helper">Provides simplified APIs for writing mods.</param>
        public override void Entry(IModHelper helper)
        {
            helper.Events.Input.ButtonsChanged += this.OnButtonsChanged;
            helper.Events.Player.InventoryChanged += this.OnInventoryChanged;
            helper.Events.GameLoop.OneSecondUpdateTicked += this.OnOneSecondUpdateTicked;
        }

        private Item lastItemDeleted;

        /*********
        ** Private methods
        *********/
        /// <summary>Raised after the player presses a button on the keyboard, controller, or mouse.</summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event data.</param>
        private void OnButtonsChanged(object sender, ButtonsChangedEventArgs e)
        {
            // ignore if player hasn't loaded a save yet
            if (!Context.IsWorldReady)
                return;

            // print button presses to the console window
            //this.Monitor.Log($"{Game1.player.Name}: {Game1.player.CursorSlotItem}.", LogLevel.Debug);

            if (KeybindList.Parse("LeftControl + Q").JustPressed())
            {
                Dropper.DropMany();
            } else if (KeybindList.Parse("Q").JustPressed())
            {
                Dropper.DropOne();
            }

            if (KeybindList.Parse("LeftControl + Z").JustPressed() && lastItemDeleted != null)
            {
                if (Game1.player.couldInventoryAcceptThisItem(lastItemDeleted))
                {
                    Game1.player.addItemToInventoryBool(lastItemDeleted);
                    this.Monitor.Log($"{Game1.player.Name}: Undone trash of {lastItemDeleted.DisplayName} ({lastItemDeleted.Stack}).", LogLevel.Debug);
                    lastItemDeleted = null;
                    Game1.playSound("pickUpItem");
                }
            }

            if (KeybindList.Parse("MouseMiddle").JustPressed())
            {
                if (Game1.activeClickableMenu is ItemGrabMenu m && m.context is Chest c)
                {
                    ItemGrabMenu.organizeItemsInList(c.items);
                }
                if (!Context.CanPlayerMove)
                    return;
                ItemGrabMenu.organizeItemsInList(Game1.player.Items);
                Game1.playSound("Ship");
            }
        }

        private void OnInventoryChanged(object sender, InventoryChangedEventArgs e)
        {
            if (!Util.IsInventory(Game1.activeClickableMenu))
                return;

            IEnumerator enumerator = e.Removed.GetEnumerator();
            enumerator.MoveNext();
            Item item = null;
            try
            {
                item = (Item)enumerator.Current;
            }
            catch (System.Exception)
            {
                this.Monitor.Log("No item stack was removed, ignoring.", LogLevel.Trace);
            }

            if (this.Helper.Input.IsDown(SButton.MouseLeft) && this.Helper.Input.IsDown(SButton.LeftAlt))
            {
                if (item != null && item.canBeTrashed())
                {
                    this.Monitor.Log($"{Game1.player.Name}: {item.DisplayName} ({item.Stack}) removed.", LogLevel.Debug);
                    Game1.player.CursorSlotItem = null;
                    lastItemDeleted = item;
                    Game1.playSound("trashcan");
                }
            }
        }

        private void OnOneSecondUpdateTicked(object sender, OneSecondUpdateTickedEventArgs e)
        {
            if (!Context.IsWorldReady)
                return;
            if (!Context.IsMultiplayer)
                return;
            if (!Context.IsOnHostComputer)
                return;
            List<ChatMessage> messages = this.Helper.Reflection.GetField<List<ChatMessage>>(Game1.chatBox, "messages").GetValue();
            if (messages.Count > 0)
            {
                string lastMessage = ChatMessage.makeMessagePlaintext(messages[messages.Count - 1].message, false);
                string[] args = lastMessage.Split(' ');
                string[] farmerNameArray = new string[args.Length - 3];
                string farmerName;
                Farmer transferrer = null;
                Farmer recipient = null;

                //if (!args[0].StartsWith(Game1.player.Name)) return;

                if (lastMessage != null && args[1] == "!pay")
                {
                    int transferAmount;
                    bool res = System.Int32.TryParse(args[args.Length - 2], out transferAmount);
                    if (!res)
                    {
                        Game1.chatBox.addMessage($"{args[args.Length - 2]} is not a valid amount!", Microsoft.Xna.Framework.Color.IndianRed);
                        return;
                    }
                    for (int i = 2; i < args.Length - 2; i++)
                    {
                        farmerNameArray[i - 2] = args[i];
                    }
                    farmerName = string.Join(" ", farmerNameArray);
                    farmerName = farmerName.Substring(0, farmerName.Length - 1);
                    this.Monitor.Log(farmerName, LogLevel.Debug);
                    foreach (var player in Game1.getAllFarmers())
                    {
                        this.Monitor.Log($"{Game1.player.Name}: {player.Name}", LogLevel.Debug);
                        if (args[0].ToLower().StartsWith(player.Name.ToLower())) transferrer = player;
                        if (player.Name.ToLower() == farmerName.ToLower())
                        {
                            recipient = player;
                        }
                    }
                    if (recipient == null || transferrer == null)
                    {
                        Game1.chatBox.addMessage($"Cannot find {farmerName}.", Microsoft.Xna.Framework.Color.IndianRed);
                        return;
                    }
                    if (transferAmount > transferrer.Money)
                    {
                        Game1.chatBox.addMessage("You do not have enough money!", Microsoft.Xna.Framework.Color.IndianRed);
                        return;
                    }
                    Game1.chatBox.addMessage($"Transferring {transferAmount} gold from {transferrer.Name} to {recipient.Name}.", Microsoft.Xna.Framework.Color.Khaki);
                    Game1.player.team.SetIndividualMoney(transferrer, transferrer.Money - transferAmount);
                    Game1.player.team.SetIndividualMoney(recipient, recipient.Money + transferAmount);
                    //player.Money = player.Money + transferAmount;
                    return;
                    //Game1.chatBox.addMessage($"Cannot find {args[2]}.", Microsoft.Xna.Framework.Color.IndianRed);
                }
            }
        }
    }
}
