﻿using StardewModdingAPI;
using StardewValley;

namespace ItemTweaks
{
    public class Dropper
    {

        public static void DropMany()
        {
            Item item = null;
            if (Context.IsPlayerFree)
            {
                item = Game1.player.CurrentItem;
            }
            else if (Util.IsInventory(Game1.activeClickableMenu))
            {
                item = Game1.player.CursorSlotItem;
                Game1.player.CursorSlotItem = null;
            }
            if (item != null && item.canBeDropped())
            {
                Game1.playSound("throwDownITem");
                Game1.createItemDebris(item, Game1.player.getStandingPosition(), Game1.player.FacingDirection).DroppedByPlayerID.Value = Game1.player.UniqueMultiplayerID;
                Game1.player.removeItemFromInventory(item);
            }
        }

        public static void DropOne()
        {
            Item item = null;
            bool inventory = Util.IsInventory(Game1.activeClickableMenu);
            if (Context.IsPlayerFree)
            {
                item = Game1.player.CurrentItem;
            }
            else if (inventory)
            {
                item = Game1.player.CursorSlotItem;
            }
            if (item != null && item.canBeDropped())
            {
                Game1.playSound("throwDownITem");
                Game1.createItemDebris(item.getOne(), Game1.player.getStandingPosition(), Game1.player.FacingDirection).DroppedByPlayerID.Value = Game1.player.UniqueMultiplayerID;
                if (inventory)
                {
                    Game1.player.CursorSlotItem.Stack = item.Stack - 1;
                    if (Game1.player.CursorSlotItem.Stack == 0)
                    {
                        Game1.player.CursorSlotItem = null;
                    }
                } else
                {
                    Game1.player.reduceActiveItemByOne();
                }
            }
        }
    }
}

